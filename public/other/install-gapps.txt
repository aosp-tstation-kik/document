$ ./install-gapp.sh 
INFO: checking prerequisites.
INFO: create gapps-config.txt
INFO: mkdir /work on target
INFO: /work does not exist on target
INFO: /work <-- install scripts and archive
[100%] /work/installer.sh
[100%] /work/gapps-config.txt
[100%] /work/gapp.zip
INFO: /tmp <-- executables etc
[100%] /tmp/app_densities.txt
[100%] /tmp/app_sizes.txt
[100%] /tmp/bkup_tail.sh
[100%] /tmp/busybox-arm
[100%] /tmp/g.prop
[100%] /tmp/gapps-remove.txt
[100%] /tmp/tar-arm
[100%] /tmp/unzip-arm
[100%] /tmp/zip-arm
chmod: /tmp/test.sh: No such file or directory
INFO: start installation
ui_print  
ui_print
ui_print ##############################
ui_print
ui_print   _____   _____   ___   ____  
ui_print
ui_print  /  _  \ |  __ \ / _ \ |  _ \ 
ui_print
ui_print |  / \  || |__) | |_| || | \ \
ui_print
ui_print | |   | ||  ___/|  __/ | | | |
ui_print
ui_print |  \ /  || |    \ |__  | | | |
ui_print
ui_print  \_/ \_/ |_|     \___| |_| |_|
ui_print
ui_print        ___   _   ___ ___  ___ 
ui_print
ui_print       / __| /_\ | _ \ _ \/ __|
ui_print
ui_print      | (_ |/ _ \|  _/  _/\__ \
ui_print
ui_print       \___/_/ \_\_| |_|  |___/
ui_print
ui_print ##############################
ui_print
ui_print  
ui_print
ui_print Open GApps pico 4.4 - 20190430
ui_print
ui_print  
ui_print
ui_print - Mounting  /cache /data /system
ui_print
ui_print  
ui_print
set_progress 0.01
ui_print - Gathering device & ROM information
ui_print
ui_print  
ui_print
/work/installer.sh: line 1343: getprop: not found
/work/installer.sh: line 1410: getprop: not found
/work/installer.sh: line 1440: getprop: not found
awk: /system/etc/permissions/android.hardware.camera.front.xml: No such file or directory
awk: /system/etc/permissions/android.hardware.camera.xml: No such file or directory
awk: /system/vendor/etc/permissions/android.hardware.camera.front.xml: No such file or directory
awk: /system/vendor/etc/permissions/android.hardware.camera.xml: No such file or directory
grep: /system/vendor/etc/: No such file or directory
/work/installer.sh: line 1546: getprop: not found
set_progress 0.03
ash: bad number
sed: /system/addon.d/70-gapps.sh: No such file or directory
set_progress 0.04
ui_print - Performing system space calculations
ui_print
ui_print  
ui_print
du: /system/app/Books.apk: No such file or directory
du: /system/app/CalendarGooglePrebuilt.apk: No such file or directory
du: /system/app/Chrome.apk: No such file or directory
du: /system/app/CloudPrint2.apk: No such file or directory
du: /system/app/Drive.apk: No such file or directory
du: /system/app/Duo.apk: No such file or directory
du: /system/app/FaceLock.apk: No such file or directory
du: /system/app/GoogleCalendarSyncAdapter.apk: No such file or directory
du: /system/app/GoogleCamera.apk: No such file or directory
du: /system/app/GoogleContactsSyncAdapter.apk: No such file or directory
du: /system/app/GoogleHome.apk: No such file or directory
du: /system/app/GoogleTTS.apk: No such file or directory
du: /system/app/GoogleVrCore.apk: No such file or directory
du: /system/app/LatinImeGoogle.apk: No such file or directory
du: /system/app/Maps.apk: No such file or directory
du: /system/app/Music2.apk: No such file or directory
du: /system/app/Newsstand.apk: No such file or directory
du: /system/app/Photos.apk: No such file or directory
du: /system/app/PlayGames.apk: No such file or directory
du: /system/app/PrebuiltBugle.apk: No such file or directory
du: /system/app/PrebuiltDeskClockGoogle.apk: No such file or directory
du: /system/app/PrebuiltExchange3Google.apk: No such file or directory
du: /system/app/PrebuiltGmail.apk: No such file or directory
du: /system/app/PrebuiltKeep.apk: No such file or directory
du: /system/app/TranslatePrebuilt.apk: No such file or directory
du: /system/app/Videos.apk: No such file or directory
du: /system/app/Wallet.apk: No such file or directory
du: /system/app/YouTube.apk: No such file or directory
du: /system/app/talkback.apk: No such file or directory
du: /system/etc/permissions/com.google.android.camera2.xml: No such file or directory
du: /system/etc/permissions/com.google.android.maps.xml: No such file or directory
du: /system/etc/permissions/com.google.android.media.effects.xml: No such file or directory
du: /system/etc/permissions/com.google.widevine.software.drm.xml: No such file or directory
du: /system/etc/preferred-apps/google.xml: No such file or directory
du: /system/lib/libfacelock_jni.so: No such file or directory
du: /system/lib/libfilterpack_facedetect.so: No such file or directory
du: /system/lib/libfrsdk.so: No such file or directory
du: /system/lib/libjni_latinimegoogle.so: No such file or directory
du: /system/priv-app/GoogleBackupTransport.apk: No such file or directory
du: /system/priv-app/GoogleFeedback.apk: No such file or directory
du: /system/priv-app/GoogleLoginService.apk: No such file or directory
du: /system/priv-app/GoogleOneTimeInitializer.apk: No such file or directory
du: /system/priv-app/GooglePartnerSetup.apk: No such file or directory
du: /system/priv-app/GoogleServicesFramework.apk: No such file or directory
du: /system/priv-app/Phonesky.apk: No such file or directory
du: /system/priv-app/PrebuiltGmsCore.apk: No such file or directory
du: /system/priv-app/SetupWizard.apk: No such file or directory
du: /system/priv-app/Velvet.apk: No such file or directory
du: /system/usr/srec/en-US/: No such file or directory
du: /system/vendor/pittpatt/: No such file or directory
set_progress 0.05
du: app/BooksStub.apk: No such file or directory
du: app/BookmarkProvider.apk: No such file or directory
du: app/CalendarGoogle.apk: No such file or directory
du: app/CloudPrint.apk: No such file or directory
du: app/DeskClockGoogle.apk: No such file or directory
du: app/EditorsDocsStub.apk: No such file or directory
du: app/EditorsSheetsStub.apk: No such file or directory
du: app/EditorsSlidesStub.apk: No such file or directory
du: app/Gmail.apk: No such file or directory
du: app/Gmail2.apk: No such file or directory
du: app/GoogleCalendar.apk: No such file or directory
du: app/GoogleCloudPrint.apk: No such file or directory
du: app/GoogleHangouts.apk: No such file or directory
du: app/GoogleKeep.apk: No such file or directory
du: app/GoogleLatinIme.apk: No such file or directory
du: app/Keep.apk: No such file or directory
du: app/NewsstandStub.apk: No such file or directory
du: app/PartnerBookmarksProvider.apk: No such file or directory
du: app/PrebuiltBugleStub.apk: No such file or directory
du: app/PrebuiltKeepStub.apk: No such file or directory
du: app/QuickSearchBox.apk: No such file or directory
du: app/Vending.apk: No such file or directory
du: priv-app/GmsCore.apk: No such file or directory
du: priv-app/GoogleNow.apk: No such file or directory
du: priv-app/GoogleSearch.apk: No such file or directory
du: priv-app/GoogleHangouts.apk: No such file or directory
du: priv-app/OneTimeInitializer.apk: No such file or directory
du: priv-app/QuickSearchBox.apk: No such file or directory
du: priv-app/Vending.apk: No such file or directory
du: priv-app/Velvet_update.apk: No such file or directory
du: priv-app/GmsCore_update.apk: No such file or directory
du: app/CanvasPackageInstaller.apk: No such file or directory
du: app/ConfigUpdater.apk: No such file or directory
du: app/GoogleBackupTransport.apk: No such file or directory
du: app/GoogleFeedback.apk: No such file or directory
du: app/GoogleLoginService.apk: No such file or directory
du: app/GoogleOneTimeInitializer.apk: No such file or directory
du: app/GooglePartnerSetup.apk: No such file or directory
du: app/GoogleServicesFramework.apk: No such file or directory
du: app/OneTimeInitializer.apk: No such file or directory
du: app/Phonesky.apk: No such file or directory
du: app/PrebuiltGmsCore.apk: No such file or directory
du: app/SetupWizard.apk: No such file or directory
du: app/Velvet.apk: No such file or directory
du: /system/lib/libjni_latinime.so: No such file or directory
du: /system/lib/libjni_latinimegoogle.so: No such file or directory
du: app/CalculatorGoogle: No such file or directory
du: priv-app/GoogleHome.apk: No such file or directory
du: priv-app/Hangouts.apk: No such file or directory
du: priv-app/PrebuiltExchange3Google.apk: No such file or directory
du: priv-app/talkback.apk: No such file or directory
du: priv-app/Wallet.apk: No such file or directory
du: etc/g.prop: No such file or directory
du: addon.d/70-gapps.sh: No such file or directory
set_progress 0.07
set_progress 0.09
set_progress 0.11
set_progress 0.13
ui_print - Removing existing/obsolete Apps
ui_print
ui_print  
ui_print

find: /system/vendor/pittpatt: No such file or directory
find: /system/usr/srec: No such file or directory
find: /system/etc/preferred-apps: No such file or directory
ui_print - Installing core GApps
ui_print
set_progress 0.15
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/defaultetc-common.tar.lz  
Found Core/defaultetc-common DPI path: unknown
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/defaultframework-common.tar.lz  
Found Core/defaultframework-common DPI path: unknown
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/googlebackuptransport-all.tar.lz  
Found Core/googlebackuptransport-all DPI path: googlebackuptransport-all/nodpi
/tmp/tar-arm: googlebackuptransport-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/googlecontactssync-all.tar.lz  
Found Core/googlecontactssync-all DPI path: googlecontactssync-all/nodpi
/tmp/tar-arm: googlecontactssync-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/googlefeedback-all.tar.lz  
Found Core/googlefeedback-all DPI path: googlefeedback-all/nodpi
/tmp/tar-arm: googlefeedback-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/googleonetimeinitializer-all.tar.lz  
Found Core/googleonetimeinitializer-all DPI path: googleonetimeinitializer-all/nodpi
/tmp/tar-arm: googleonetimeinitializer-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/googlepartnersetup-all.tar.lz  
Found Core/googlepartnersetup-all DPI path: googlepartnersetup-all/nodpi
/tmp/tar-arm: googlepartnersetup-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/gmscore-arm.tar.lz  
Found Core/gmscore-arm DPI path: gmscore-arm/160
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/gsfcore-all.tar.lz  
Found Core/gsfcore-all DPI path: gsfcore-all/nodpi
/tmp/tar-arm: gsfcore-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/vending-arm.tar.lz  
Found Core/vending-arm DPI path: vending-arm/nodpi
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/gsflogin-all.tar.lz  
Found Core/gsflogin-all DPI path: gsflogin-all/nodpi
/tmp/tar-arm: gsflogin-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Core/setupwizard-all.tar.lz  
Found Core/setupwizard-all DPI path: setupwizard-all/nodpi
/tmp/tar-arm: setupwizard-all/common: Not found in archive
/tmp/tar-arm: Exiting with failure status due to previous errors
ui_print  
ui_print
set_progress 0.25
ui_print - Installing swypelibs
ui_print
Archive:  /work/gapp.zip
signed by SignApk
 extracting: /tmp/Optional/swypelibs-lib-arm.tar.lz  
Found Optional/swypelibs-lib-arm DPI path: unknown
set_progress 0.30
ui_print  
ui_print
ui_print - Miscellaneous tasks
ui_print
ui_print  
ui_print
set_progress 0.80
set_progress 0.85
find: /system/vendor/pittpatt: No such file or directory
set_progress 0.92
set_progress 0.94
set_progress 0.96
ui_print - Copying Log to /work
ui_print
ui_print  
ui_print
set_progress 0.97
ui_print - Installation complete!
ui_print
ui_print  
ui_print
set_progress 0.98
cp: can't stat '/system/default.prop': No such file or directory
cp: can't stat '/vendor/build.prop': No such file or directory
cp: can't stat '/data/local.prop': No such file or directory
cp: can't stat '/build.prop': No such file or directory
/work/installer.sh: line 2573: logcat: not found
set_progress 1.0
ui_print - Unmounting  /cache /data /system
ui_print
ui_print  
ui_print
INFO: get install log
[100%] /work/open_gapps_debug_logs.tar.gz
INFO: done
$
